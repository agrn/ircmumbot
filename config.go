package main

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	IrcEnableSSL, IrcDisableCertificateVerification, MumbleDisableCertificateVerification bool
	IrcServer, MumbleServer                                                               Server
	ChannelMapping                                                                        map[string]string
}

type Server struct {
	Host string
	Port uint16
}

func LoadConfiguration(configFile string) (Config, error) {
	config := Config{
		IrcEnableSSL:                         false,
		IrcDisableCertificateVerification:    false,
		MumbleDisableCertificateVerification: false,
		IrcServer: Server{
			Host: "192.168.0.149",
			Port: 6667,
		},
		MumbleServer: Server{
			Host: "localhost",
			Port: 64738,
		},
		ChannelMapping: map[string]string{"#test": "root/test"},
	}
	content, err := ioutil.ReadFile(configFile)

	if err != nil {
		return config, err
	}

	err = json.Unmarshal(content, &config)
	return config, err
}
