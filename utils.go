package main

func StringSliceContains(slice []string, value string) bool {
	for _, sliceValue := range slice {
		if sliceValue == value {
			return true
		}
	}

	return false
}
