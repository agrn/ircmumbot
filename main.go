package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/husio/irc"
)

func handleMessage(message *irc.Message) {
	fmt.Println(message.Trailing)
}

func main() {
	var configFile string
	loop := true
	sigs := make(chan os.Signal)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	flag.StringVar(&configFile, "c", "/etc/ircmumbot.json", "Configuration file")
	flag.Parse()

	config, err := LoadConfiguration(configFile)
	if err != nil {
		fmt.Println(err)
	}

	if err = IrcConnectToServer(config); err != nil {
		fmt.Println(err)
	} else {
		for loop {
			select {
			case message := <-IrcMessagesChannel:
				handleMessage(message)
			case <-sigs:
				IrcConnection.Send("QUIT :ircmumbot stopped")
				IrcConnection.Close()
				loop = false
			case <-IrcDisconnectChannel:
				loop = false
			}
		}
	}
}
