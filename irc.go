package main

import (
	"fmt"

	"github.com/husio/irc"
)

var (
	IrcConnection        *irc.Conn
	IrcChannels          []string
	IrcDisconnectChannel chan bool
	IrcMessagesChannel   chan *irc.Message
)

func IrcConnectToServer(config Config) (err error) {
	address := fmt.Sprintf("%s:%d", config.IrcServer.Host, config.IrcServer.Port)
	IrcConnection, err = irc.Connect(address)
	if err != nil {
		return
	}

	IrcConnection.Send("USER ircmumbot %s * :ircmumbot", address)
	IrcConnection.Send("NICK ircmumbot")
	IrcJoinChannels(config.ChannelMapping)

	IrcDisconnectChannel = make(chan bool)
	IrcMessagesChannel = make(chan *irc.Message)

	go IrcLoop()

	return
}

func IrcJoinChannels(channelMapping map[string]string) {
	for _, channel := range IrcChannels {
		if _, ok := channelMapping[channel]; !ok {
			IrcConnection.Send("PART %s", channel)
		}
	}

	for channel, _ := range channelMapping {
		if !StringSliceContains(IrcChannels, channel) {
			IrcConnection.Send("JOIN %s", channel)
		}
	}
}

func IrcLoop() {
	for {
		message, err := IrcConnection.ReadMessage()
		if err != nil {
			fmt.Println("Cannot read message: %s", err)
			break
		}

		switch message.Command {
		case "PING":
			IrcConnection.Send("PONG %s", message.Trailing)
		case "PRIVMSG":
			IrcMessagesChannel <- message
		}
	}

	IrcConnection.Close()
	IrcDisconnectChannel <- true
}
